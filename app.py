#FLASK
from flask import Flask, jsonify, render_template, request
import os,optparse,sys
app = Flask(__name__)

developer = os.getenv("DEVELOPER", "Me")
environment=os.getenv("ENVIRONMENT","development")

alumnos = {
    "Marcela Melgar": 18,
    "Jose Reyes": 19,
    "Nickolas Nolte": 19,
    "Javier Mazariegos": 18,
    "Daniel Behar": 19,
    "Esteban Samayoa": 19,
    "Fatima Avila": 18,
    "Cruz del Cid": 18,
    "Lorena Perez": 19,
    "Juan Luis Fernandez": 19,
    "Rodrigo Reyes": 19,
    "Carlos Alvarado": 19,
    "Mario Pisquiy": 18
}

@app.route("/alumnos")
def api_students():
    return jsonify(alumnos)


@app.route("/ages")
def ages():
    edades=[edad for estudiante,edad in alumnos.items()] #list comprehension
    average=int(sum(edades)/len(edades))
    return render_template("ages.html", alumnos=alumnos, average=average)


@app.route("/students")
def students():
    return render_template("students.html", alumnos=alumnos)


def search_student(student):
    """
    A function that searches in data source
    """
    result=[]
    for name,age in alumnos.items():
        if student is not None:
            if student.lower() in name.lower():
                result.append(name)

    print(f"Result {result}")
    return result


@app.route("/search",methods=["GET"]) #xss http://0.0.0.0:5000/search?student=a #query parameters
def search():
    """
    /search route, to search from an input form.
    """
    student_to_find=request.args.get("student", None)
    print(f"A buscar: {student_to_find}")
    student_list=search_student(student_to_find)
    return render_template("search.html",student_list_result=student_list)

@app.route("/")
def home():
    foo="bar"
    return render_template("home.html", mivariable=foo ,developer=developer)

if __name__ == "__main__":
    debug=False
    if environment == "development" or environment == "local":
        debug=True
    print("Local change")
    app.run(host="0.0.0.0",debug=debug)
